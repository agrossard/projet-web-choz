import { useState, useEffect } from 'react';
import { Redirect } from 'react-router-dom';
import PublicationsList from './PublicationsList';
import isLoggedIn from './is_logged_in';

export default function Feeds(props) {
	if (!isLoggedIn()) {
		return <Redirect to="/login" />;
	}

	const [publications, setPublications] = useState([]);

	const classNames = ` ${publications?.length ? '' : 'is-loading'}`;

	function mountPublications() {
		fetch('http://localhost:8080/publications/random', {
			credentials: 'include',
			headers: {
				Accept: 'application/json',
				'Content-Type': 'application/json',
			},
		})
			.then(response => response.json())
			.then(data => {
				setPublications(data);
			})
			.catch(err => alert(err));
	}

	useEffect(mountPublications, []);

	return (
		<div className={classNames}>
			<PublicationsList publications={publications} />
		</div>
	);
}

