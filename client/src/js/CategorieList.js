import { useState, useEffect } from 'react';
import { Redirect } from 'react-router-dom';
import CategorieThumbnail from './CategorieThumbnail';
import isLoggedIn from './is_logged_in';

/**
 *à partir d'une liste de vidéo, récupère et affiche les catégories
 * @param {*} props
 * @returns
 */
export default function CategorieList(props) {
	if (!isLoggedIn()) {
		return <Redirect to="/login" />;
	}
	const [categories, setCategorie] = useState([]);
	const categorieClass = `categorieList ${
		categories?.length ? '' : 'is-loading'
	}`;
	/* recupération des données*/
	function mountImagesCategories() {
		fetch(`http://localhost:8080/categories`, {
			credentials: 'include',
		})
			.then(response => response.json())
			.then(data => setCategorie(data));
	}

	useEffect(mountImagesCategories, []);

	return (
		<div className="container">
			<div className="row">
				<div className="col-12">
					<div className={categorieClass}>
						{categories.map(categorie => (
							<CategorieThumbnail categorie={categorie} key={categorie.id} />
						))}
					</div>
				</div>
			</div>
		</div>
	);
}
