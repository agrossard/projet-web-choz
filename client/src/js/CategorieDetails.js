import { useState, useEffect } from 'react';
import { useParams } from 'react-router';
import { Redirect } from 'react-router-dom';
import PublicationsList from './PublicationsList';
import isLoggedIn from './is_logged_in';

/**
 *à partir d'une liste de vidéo, récupère et affiche les catégories
 * @param {*} props
 * @returns
 */
export default function CategorieDetails() {
	if (!isLoggedIn()) {
		return <Redirect to="/login" />;
	}
	const { id } = useParams();
	const [publications, setPublications] = useState([]);

	const classNames = `${publications?.length ? '' : 'is-loading'}`;

	/* recupération des données*/
	function mountCategoriesDetail() {
		fetch(`http://localhost:8080/chooz/categories`, {
			credentials: 'include',
			headers: {
				Accept: 'application/json',
				'Content-Type': 'application/json',
			},
			method: 'POST',
			body: JSON.stringify({ id_categorie: id }),
		})
			.then(result => result.json())
			.then(data => {
				setPublications(data);
			});
	}

	useEffect(mountCategoriesDetail, []);

	return (
		<div className={classNames}>
			<PublicationsList key="1" publications={publications} />
		</div>
	);
}
