import { useState, useRef } from 'react';
import { Redirect } from 'react-router-dom';
import CategorieList from './CategorieList';
import PublicationList from './PublicationsList';
import isLoggedIn from './is_logged_in';

export default function Explorer() {
	if (!isLoggedIn()) {
		return <Redirect to="/login" />;
	}
	const searchInput = useRef();

	const [publications, setPublications] = useState([]);
	const [words, setWords] = useState('');

	function submitSearchBarText() {
		event.preventDefault();

		fetch(`http://localhost:8080/publications/search`, {
			credentials: 'include',
			headers: {
				Accept: 'application/json',
				'Content-Type': 'application/json',
			},
			method: 'POST',
			body: JSON.stringify({ words: searchInput.current.value }),
		})
			.then(result => result.json())
			.then(data => {
				if (!data.message) {
					setPublications(data);
				}
				setWords(searchInput.current.value);
			})
			.catch(err => {
				console.log(err);
			});
	}

	let explorer_classNames = ` ${words?.length ? '' : 'is-loading'}`;
	let categories_classNames = ` ${words?.length ? 'is-loading' : ''}`;

	return (
		<div className="container">
			<div className="row justify-content-center">
				<div className="col-sm-6 col-12">
					<form
						className="form-group input-group form-margin"
						onSubmit={event => submitSearchBarText(event)}
					>
						<input
							id="search_bar"
							type="search"
							className="form-control form-group "
							placeholder="Search"
							aria-label="Search"
							aria-describedby="search-addon"
							ref={searchInput}
						/>
						<button
							className="input-group-text border-0 form-group "
							id="search-addon"
						>
							<i className="fas fa-search"></i>
						</button>
					</form>
				</div>
			</div>
			<div className={categories_classNames}>
				<CategorieList />
			</div>
			<div className={explorer_classNames}>
				<PublicationList publications={publications} />
			</div>
		</div>
	);
}
