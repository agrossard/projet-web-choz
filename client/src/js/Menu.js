import { useHistory, Link } from 'react-router-dom';
import { useParams } from 'react-router';
import store from 'store';
import isLoggedIn from './is_logged_in';

export default function Menu() {
	const id = store.get('id');
	const history = useHistory();
	const linkToProfile = '/choozer/' + id;
	/**
	 * Permet de deconnecter un utilisateur
	 */
	function deconnexion() {
		fetch('http://localhost:8080/users/logout')
			.then(response => {
				store.set('loggedIn', false);
				store.set('id', '');
				history.push('/login');
			})
			.catch(err => console.log(err));
	}

	return (
		<header>
			<nav className="navbar navbar-expand-lg navbar-dark bg-dark">
				<div className="navbar-nav ml-auto">
					<div className="container">
						<div className="row">
							<div id="logo" className="col-sm-3">
								<h1>
									<Link to="/">
										<img src="/assets/icons/chooz.svg" width="80" />
									</Link>
								</h1>
							</div>
							<div className="col-8 col-sm-6" id="navbarResponsive">
								<div className="justify-content-center text-center align-items-center mx-auto">
									<div className="row">
										<div className="col-5">
											<Link
												to="/feeds"
												className="nav-item nav-link active house-icon-float"
											>
												<i className="fas fa-house fa-lg"></i>
											</Link>
										</div>
										<div className="col-3 ">
											<Link to="/" className="nav-item nav-link active">
												<img src="/assets/icons/logo.svg" width="30px" />
											</Link>
										</div>
										<div className="col-4 ">
											<Link
												to="/explorer"
												className="nav-item nav-link active search-icon-float"
											>
												<i className="fas fa-search fa-lg"></i>
											</Link>
										</div>
									</div>
								</div>
							</div>
							<div className="col-4 col-sm-3 text-right">
								<Link to={linkToProfile}>
									<i className="fas fa-user fa-lg"></i>
								</Link>
								{isLoggedIn && (
									<button className="btn btn-link" onClick={deconnexion}>
										<i className="fas fa-sign-out-alt fa-lg" id="btn-logout"></i>
									</button>
								)}
							</div>
						</div>
					</div>
				</div>
			</nav>
		</header>
	);
}
