import { Link } from 'react-router-dom';
/**
 * 
 * @param {*} param0 
 * @returns 
 */
const CategorieThumbnail = ({ categorie: { name, thumbnail, id } }) => (
	
	<Link to={`/categories/${id}`}>
		<div className="col-sm-4 categorie-form">
			<div className="col-sm-auto">
				<button
					type="button"
					className="btn-categorie btn btn-primary buttonThumbnail"
				>
					{name}
				</button>
			</div>
			<img
				src={`https://source.unsplash.com/${thumbnail}/600x340`}
				className="img-fluid img-categorie"
			/>
		</div>
	</Link>
);

export default CategorieThumbnail;
