import PublicationDelete from './PublicationDelete';
import isLoggedIn from './is_logged_in';

export default function PublicationExifs(props) {
	if (!isLoggedIn()) {
		return <Redirect to="/login" />;
	}
	const { name, type, size, lastModifiedDate } = JSON.parse(props.exifs);
	const date = new Intl.DateTimeFormat('fr-FR', {
		year: 'numeric',
		month: '2-digit',
		day: '2-digit',
		hour: '2-digit',
		minute: '2-digit',
	}).format(props.date * 1000);

	function bytesToSize(bytes) {
		var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
		if (bytes == 0) return 'n/a';
		var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
		if (i == 0) return bytes + ' ' + sizes[i];
		return (bytes / Math.pow(1024, i)).toFixed(1) + ' ' + sizes[i];
	}

	return (
		<div className="col-2 header_publication">
			<i
				className="bi bi-three-dots exifs cursor-pointer"
				data-toggle="modal"
				data-target="#exifs"
			></i>

			<div
				className="modal fade"
				id="exifs"
				tabIndex="-1"
				role="dialog"
				aria-labelledby="exifsTitle"
				aria-hidden="true"
			>
				<div className="modal-dialog modal-dialog-centered" role="document">
					<div className="modal-content exifs_popup">
						<div className="modal-body">
							<button
								type="button"
								className="close"
								data-dismiss="modal"
								aria-label="Close"
							>
								x
							</button>
							<p>
								<span>Nom : </span>
								{name}
							</p>
							<p>
								<span>Type : </span>
								{type}
							</p>
							<p>
								<span>Taille : </span>
								{bytesToSize(size)}
							</p>
							<p>
								<span>Derniere modification de l'image : </span>
								{lastModifiedDate}
							</p>
							<p>
								<span>Création de la publication : </span>
								{date}
							</p>
						</div>
						<PublicationDelete
							className="cursor-pointer"
							id_publication={props.id_publication}
							id_user={props.id_user}
							filename={props.filename}
							
						/>
					</div>
				</div>
			</div>
		</div>
	);
}
