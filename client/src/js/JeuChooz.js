import { Redirect } from 'react-router-dom';
import isLoggedIn from './is_logged_in';
import { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
/**
 *
 * @returns
 */
export default function JeuChooz() {
	if (!isLoggedIn()) {
		return <Redirect to="/login" />;
	}
	const history = useHistory();
	const [game, setGame] = useState({ id: 0, pub_first: {}, pub_second: {} });
	/**
	 *
	 * @param {*} value
	 */
	function handleClickOnImage(value) {
		/*Fonction pour  gérer le clic sur une image*/
		let data = {
			id_game: game.id,
			id_publication: value,
		};
		/* envoie la photo choisie*/
		fetch(`http://localhost:8080/chooz`, {
			credentials: 'include',
			headers: {
				Accept: 'application/json',
				'Content-Type': 'application/json',
			},
			method: 'POST',
			body: JSON.stringify(data),
		}).then(data => {
			mountGame();
		});
	}

	function mountGame() {
		/*connexion et recupération des méta-données pour gerer le jeu*/
		fetch(`http://localhost:8080/game`, {
			credentials: 'include',
			headers: {
				Accept: 'application/json',
				'Content-Type': 'application/json',
			},
		})
			.then(response => response.json())
			.then(data => {
				if (data.message) {
					history.push('/publications/add');
				}
				setGame(data[0]);
			})
			.catch(error => {
				alert(error);
			});
	}

	useEffect(mountGame, []);

	return (
		<div className="container">
			<div className="row justify-content-center">
				<div className="col-sm-6 col-12 text-center">
					<h4>Faut que ça chooz !</h4>
				</div>
			</div>

			<div className="row justify-content-center">
				<div className="col-sm-6 cursor-pointer">
					<div className="justify-content-center">
						<img
							className="img-fluid rounded imageChooz"
							src={`/assets/images/publications/${game.pub_first.pathfile}`}
							alt="Card image cap"
							key={game.pub_first.id}
							onClick={() => handleClickOnImage(game.pub_first.id)}
						/>
					</div>
				</div>
			</div>
			<div className="row  justify-content-center">
				<div className="col-sm-6 cursor-pointer">
					<div className="justify-content-center">
						<img
							className=".img-fluid rounded imageChooz"
							src={`/assets/images/publications/${game.pub_second.pathfile}`}
							alt="Card image cap"
							key={game.pub_second.id}
							onClick={() => handleClickOnImage(game.pub_second.id)}
						/>
					</div>
				</div>
			</div>
		</div>
	);
}
