import { Link } from 'react-router-dom';
import PostForm from './PostForm';

export default function AddButton() {
	return (
		<div className="container">
			<div className="row justify-content-right">
				<div className=" add_button_style">
					<Link to="/publications/add">
						<img src="/assets/icons/add.svg" className="add_button_style" />
					</Link>
				</div>
			</div>
		</div>
	);
}
