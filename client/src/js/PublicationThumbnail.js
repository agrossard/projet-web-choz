import { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import PublicationExifs from './PublicationExifs';
import isLoggedIn from './is_logged_in';

export default function PublicationThumbail(props) {
	if (!isLoggedIn()) {
		return <Redirect to="/login" />;
	}
	const { user, categories, id, description, pathfile, date, meta_data } =
		props.publication;
	const img_user =
		'/assets/images/users/' +
		(user.thumbnail == null ? 'user.png' : user.thumbnail);
	const img_publication = '/assets/images/publications/' + pathfile;

	const history = useHistory();

	const [chooz, setChooz] = useState(0);

	function fetchChooz() {
		fetch('http://localhost:8080/chooz/publication', {
			credentials: 'include',
			headers: {
				Accept: 'application/json',
				'Content-Type': 'application/json',
			},
			method: 'POST',
			body: JSON.stringify({ id_publication: id }),
		})
			.then(response => response.json())
			.then(data => {
				setChooz(data[0].chooz);
			})
			.catch(err => alert(err));
	}
	useEffect(fetchChooz, []);

	function redirectToUserProfile() {
		history.push('/choozer/' + user.id);
	}

	function redirectToCategorie(id) {
		history.push('/categories/' + id);
	}

	return (
		<div className="publication container">
			<div className="row align-items-center justify-content-center">
				<div className="col-sm-8 col-12">
					<div className="row">
						<div className="col-12">
							<div className="col-2 header_publication">
								<img src={img_user} className="img-fluid rounded-circle" />
							</div>
							<div
								className="col-8 header_publication cursor-pointer"
								onClick={() => redirectToUserProfile()}
							>
								<p className="username">{props.publication.user.username}</p>
							</div>
							<PublicationExifs
								exifs={meta_data}
								date={date}
								id_publication={id}
								id_user={user.id}
								filename={pathfile}
							/>
						</div>
					</div>
					<div className="row">
						<div className="col-12">
							<img
								src={img_publication}
								className="img-fluid img_publication"
							/>
						</div>
					</div>
					<div className="row publication_info">
						<div className="col-10">
							{categories.map(categorie => (
								<span
									key={categorie.id}
									className="badge badge-pill badge-chooz cursor-pointer"
									onClick={() => redirectToCategorie(categorie.id)}
								>
									{categorie.name}
								</span>
							))}
						</div>

						<div className="col-2">
							<span className="publication_chooz publication_texte_chooz align-middle">
								{chooz}
							</span>
							<img
								src="/assets/icons/logo_chooz.svg"
								className="align-middle img-fluid publication_chooz publication_logo_chooz"
							/>
						</div>
					</div>
					<div className="row">
						<div className="col-12">
							<p>{description}</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	);
}
