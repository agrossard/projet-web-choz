export default function CategorieButton(props) {
	return (
		<div className="col-6 col-sm-2 categorie-profil">
			<button className="btn btn-primary">{props.name}</button>
		</div>
	);
}
