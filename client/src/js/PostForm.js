import { useRef, useState, useEffect } from 'react';
import Select from 'react-select';
import { Redirect } from 'react-router-dom';
import isLoggedIn from './is_logged_in';

/**
 * Upload d'une publication
 * @returns message de validation et redirection vers le menu principal
 */
export default function PostForm() {
	// Fichier selectionné
	const [selectedFile, setSelectedFile] = useState();
	const [isSelected, setIsSelected] = useState(false);
	// Chargement du formulaire
	const [isLoading, setIsLoading] = useState(false);
	// Categories recupérés via l'API
	const [categories, setCategories] = useState([]);
	// Categorie(s) selectionnées dans le module Select
	const [selectedCategorie, setSelectedCategorie] = useState([]);
	// Au DidMount() on fetch les categories
	useEffect(fetchCategories, []);

	if (!isLoggedIn()) {
		return <Redirect to="/login" />;
	}

	/**
	 * Recupère les catégories depuis l'API
	 */
	function fetchCategories() {
		fetch('http://localhost:8080/categories', {
			credentials: 'include',
			headers: {
				Accept: 'application/json',
				'Content-Type': 'application/json',
			},
		})
			.then(response => response.json())
			.then(data => setCategories(data))
			.catch(err => console.log('Erreur : ' + err));
	}

	// Genère les options de la liste categories
	const generateOptions = categories.map(categorie => ({
		value: categorie.id,
		label: categorie.name,
	}));

	// Event de changement sur les categories
	const handleChangeCategorie = categorie => {
		if (categorie.length > 3) {
			alert('3 categories maximum');
		} else {
			setSelectedCategorie(
				Array.isArray(categorie) ? categorie.map(x => x.value) : []
			);
		}
	};

	const changeHandler = event => {
		setSelectedFile(event.target.files[0]);
		setIsSelected(true);
	};
	const descriptionInput = useRef();

	/**
	 * A la fin du formulaire, on envoi les informations à l'API
	 * @param {Evenement lors du clique} event
	 */
	function handleSubmit(event) {
		event.preventDefault();
		setIsLoading(true);

		// recupère les meta-données du fichier
		const fileInfo = {};
		fileInfo.toJSON = function () {
			return {
				lastModified: selectedFile.lastModified,
				lastModifiedDate: selectedFile.lastModifiedDate,
				name: selectedFile.name,
				size: selectedFile.size,
				type: selectedFile.type,
			};
		};

		const formData = new FormData();
		formData.append('description', descriptionInput.current.value);
		formData.append('image', selectedFile);
		formData.append('categories', JSON.stringify(selectedCategorie));
		formData.append('exifs', JSON.stringify(fileInfo));

		fetch(`http://localhost:8080/publications`, {
			method: 'POST',
			body: formData,
			credentials: 'include',
		}).then(response => response.json());
		//.then(({ id }) => history.push(`/videos/${id}`));
	}

	// Style pour la div Select
	const style = {
		control: base => ({
			...base,
			border: '2px solid #7D475A',
			borderRadius: '10px',
		}),
	};

	return (
		<div className="container">
			<div className="row align-items-center justify-content-center">
				<div className="col-sm-8">
					<form className="postForm" onSubmit={event => handleSubmit(event)}>
						<div className="form-group">
							<label className="custom-file-label" htmlFor="file">
								{isSelected ? <p>{selectedFile.name}</p> : <p> Chooz file </p>}
							</label>
							<input
								required
								type="file"
								className="custom-file-input"
								id="file"
								accept=".png, .svg, .jpeg, .jpg"
								onChange={changeHandler}
							/>
						</div>
						<div className="form-group">
							<label htmlFor="description" id="insertion">
								Description
							</label>
							<textarea
								required
								id="description"
								cols="30"
								rows="10"
								className="form-control"
								ref={descriptionInput}
							></textarea>
						</div>
						<div className="form-group">
							<label htmlFor="categories" id="insertion">
								Categories
							</label>
							<Select
								className="dropdown"
								placeholder="Select categorie(s)"
								options={generateOptions}
								onChange={handleChangeCategorie}
								styles={style}
								isMulti
								isClearable
							></Select>
						</div>
						<div className="form-group">
							<button
								type="submit"
								className="btn btn-primary"
								disabled={isLoading}
							>
								{!isLoading ? 'Envoyer' : 'Loading...'}
							</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	);
}
