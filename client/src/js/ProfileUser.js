import CategorieButton from './CategorieButton';
import { useEffect, useState } from 'react';
import { useHistory, Link, Redirect } from 'react-router-dom';
import { useParams } from 'react-router';
import PublicationsList from './PublicationsList';
import isLoggedIn from './is_logged_in';
import store from 'store';

export default function ProfileUser() {
	if (!isLoggedIn()) {
		return <Redirect to="/login" />;
	}

	const history = useHistory();

	const [categories, setCategories] = useState([]);
	const [isFollower, setisFollower] = useState(false);
	const [numberChooz, setNumberChooz] = useState([]);
	const [id_categorie, setIdCategorie] = useState(0);
	const [publications, setPublications] = useState([]);
	const [user, setUser] = useState([]);
	const { id } = useParams();

	const id_user_on_page = store.get('id');

	let isLoading = 'is-loading';
	if (id_user_on_page == id) {
		isLoading = '';
	}

	/**
	 * Retourne le nombre total de chooz de l'utilisateur
	 */
	function fetchChooz() {
		fetch(`http://localhost:8080/chooz/users`, {
			method: 'POST',
			credentials: 'include',
			headers: {
				Accept: 'application/json',
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({
				id_user: id,
			}),
		})
			.then(response => response.json())
			.then(data => {
				setNumberChooz(data.chooz);
			});
	}

	/**
	 * Permet de follow un utilisateur donné
	 */
	function fetchFollow() {
		fetch(`http://localhost:8080/follow/isFollowers`, {
			method: 'POST',
			credentials: 'include',
			headers: {
				Accept: 'application/json',
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({
				id_user: id_user_on_page,
				followers: id,
			}),
		})
			.then(result => result.json())
			.then(data => {
				setisFollower(data.message);
			});
	}

	function PostFollow() {
		let url = '';
		if (isFollower == true) {
			setisFollower(false)
			url = 'http://localhost:8080/follow/unfollow';
		} else {
			setisFollower(true)
			url = 'http://localhost:8080/follow/follow';
		}
		fetch(url, {
			method: 'POST',
			credentials: 'include',
			headers: {
				Accept: 'application/json',
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({
				id_user: id_user_on_page,
				following: id,
			}),
		})
			.then(response => response)

	}

	/**
	 * Retourne les infos de l'utilisateur
	 */
	function fetchUsers() {
		fetch(`http://localhost:8080/users/${id}`, {
			method: 'GET',
			credentials: 'include',
		})
			.then(response => response.json())
			.then(data => {
				setUser(data);
			});
	}

	/**
	 * Retourne les categories de l'utilisateur
	 */
	function fetchCategories() {
		fetch(`http://localhost:8080/categories/users`, {
			method: 'POST',
			credentials: 'include',
			headers: {
				Accept: 'application/json',
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({
				id_user: id,
			}),
		})
			.then(response => response.json())
			.then(data => {
				setCategories(data);
			});
	}
	/**
	 * Retourne les publications de l'utilisateur en fonction du click sur une categorie
	 */
	function fetchPublicationsWithCategorie(in_id_categorie) {
		if (id_categorie == in_id_categorie) {
			setIdCategorie(0);
		} else {
			setIdCategorie(in_id_categorie);
		}

		if (id_categorie != in_id_categorie) {
			fetch(`http://localhost:8080/publications/users/categories`, {
				method: 'POST',
				credentials: 'include',
				headers: {
					Accept: 'application/json',
					'Content-Type': 'application/json',
				},
				body: JSON.stringify({
					id_user: id,
					id_categorie: in_id_categorie,
				}),
			})
				.then(response => response.json())
				.then(data => {
					setPublications(data);
				})
				.catch(err => alert(err));
		} else {
			fetchPublications();
		}
	}

	/**
	 * Retourne les publications de l'utilisateur
	 */
	function fetchPublications() {
		fetch(`http://localhost:8080/publications/users`, {
			method: 'POST',
			credentials: 'include',
			headers: {
				Accept: 'application/json',
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({
				id_user: id,
			}),
		})
			.then(response => response.json())
			.then(data => {
				setPublications(data);
			})
			.catch(err => alert(err));
	}

	const classPublications = ` ${publications?.length ? '' : 'is-loading'}`;
	const classCategories = id_categorie > 0 ? 'btn-chooz' : '';

	/**
	 * btn-primary
	 * Permet de deconnecter un utilisateur
	 */
	function logout() {
		fetch(`http://localhost:8080/user/logout`, {
			credentials: 'include',
			headers: {
				Accept: 'application/json',
				'Content-Type': 'application/json',
			},
		})
			.then(response => response.json())
			.then(response => {
				store.set('loggedIn', false);
				store.set('id', '');
				history.push('/login');
			})
			.catch(err => alert(err));
	}

	useEffect(fetchCategories, [id]);
	useEffect(fetchFollow, [id]);
	useEffect(fetchUsers, [id]);
	useEffect(fetchChooz, [id]);
	useEffect(fetchPublications, [id]);

	const { username, description, thumbnail } = user;

	return (
		<div className="container">
			<div className="row justify-content-center profil-editor">
				<div className="col-12 text-center">
					<img
						src={`/assets/images/users/${
							thumbnail == null ? 'user.png' : thumbnail
						}`}
						className="img-fluid rounded-circle img-profil"
						id="img-user"
					/>
					<Link to="/choozer/edit" className={isLoading}>
						<i className="float-right fas fa-pencil fa-lg"></i>{' '}
					</Link>
				</div>
			</div>
			<div className="row justify-content-center profil-editor ">
				<div className="col-12">
					<div className="col-sm-6  categorie-profil">
						<p className="font-weight-bold text-right" id="number-chooz">
							{numberChooz}
						</p>
					</div>
					<div className="col-sm-6 categorie-profil align-middle">
						<img src="/assets/icons/logo_profile.svg" />
					</div>
				</div>
			</div>
			<div className={'row justify-content-center profil-editor' + isLoading}>
				<div className={'col-1' + (isLoading == '' ? ' is-loading' : '')}>
					<button
						className={
							'btn btn-primary ' + (isFollower == true ? 'btn-chooz' : '')
						}
						onClick={PostFollow}
					>
						<i
							className={
								'fas ' + (isFollower == true ? 'fa-user-check' : 'fa-user-plus')
							}
						></i>
					</button>
				</div>
				<div className="col-2">
					<p className="font-weight-bold h5" id="text-username">
						{username}
					</p>
				</div>
				<div className={'col-1 ' + isLoading}>
					<button className="btn btn-primary" onClick={logout}>
						<i className="fas fa-sign-out-alt"></i>
					</button>
				</div>
			</div>
			<div className="row justify-content-center profil-editor ">
				<div className="col-sm-6 col-12">
					<p className="font-weight-normal h6">{description}</p>
				</div>
			</div>
			<div className="row text-center profil-editor">
				<div className="col-12 ">
					{categories.map(categorie => (
						<div key={categorie.id} className="col-6 col-sm-2 categorie-profil">
							<button
								className={
									'btn btn-primary ' +
									(id_categorie == categorie.id ? classCategories : '')
								}
								onClick={() => fetchPublicationsWithCategorie(categorie.id)}
							>
								{categorie.name}
							</button>
						</div>
					))}
				</div>
			</div>
			<div
				id="profil_publication"
				className={'row ' + classPublications}
			>
				<div className="col-12 ">
					<PublicationsList publications={publications} />
				</div>
			</div>
		</div>
	);
}
