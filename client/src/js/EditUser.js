import { useEffect, useRef, useState } from 'react';
import store from 'store';

/**
 * Formulaire d'inscription pour un nouveau compte
 * @returns redirection et message de confirmationx
 */
export default function EditUser() {
	// chargement du formulaire
	const [selectedGender, setSelectedGender] = useState('');
	const [selectedFile, setSelectedFile] = useState();
	const [isSelected, setIsSelected] = useState(false);

	// références aux informations de l'utilisateur
	const emailInput = useRef();
	const chooserNameInput = useRef();
	const birthdateInput = useRef();
	const passwordInput = useRef();
	const profilDescriptionInput = useRef();
	const [user, setUser] = useState([]);
	const id = store.get('id');

	function loadData() {
		fetch(`http://localhost:8080/users/${id}`, {
			method: 'GET',
			credentials: 'include',
		})
			.then(response => response.json())
			.then(data => {
				setUser(data);
			});
	}

	/**
	 * A la fin du formulaire, on envoi les informations à l'API
	 * @param {Evenement lors du clique} event
	 */
	function handleSubmit(event) {
		event.preventDefault();

		// split la date pour recuperer un timestamp
		const date = birthdateInput.current.value;
		const [year, month, day] = date.split('-');

		// genere le formData que l'on envoi avec la methode post
		const formData = new FormData();
		if (chooserNameInput.current.value) {
			formData.append('username', chooserNameInput.current.value);
		}
		if (emailInput.current.value) {
			formData.append('mail', emailInput.current.value);
		}
		if (passwordInput.current.value) {
			formData.append('password', passwordInput.current.value);
		}
		if (date) {
			formData.append(
				'birthdate',
				parseInt(new Date(year, month - 1, day, 22, 0, 0).getTime() / 1000)
			);
		}
		if (selectedGender) {
			formData.append('sexe', selectedGender);
		}
		if (profilDescriptionInput.current.value) {
			formData.append('description', profilDescriptionInput.current.value);
		}
		if (selectedFile) {
			formData.append('image', selectedFile);
		}

		// envoi du formulaire d'inscription de l'utilisateur
		fetch('http://localhost:8080/users/update', {
			method: 'POST',
			body: formData,
			credentials: 'include',
		})
			.then(response => {
				if (response.ok) {
					store.set('loggedIn', true);
					store.set('id', response.user);
				}
			})
			.catch(err => {
				alert(err);
			});
	}

	/**
	 *
	 * @param {Changemement d'etat du fichier} changeEvent
	 */
	function handleFileChange(event) {
		setSelectedFile(event.target.files[0]);
		setIsSelected(true);
	}

	/**
	 *
	 * @param {Changement de radio boutton selectionné} changeEvent
	 */
	function handleGenderChange(event) {
		setSelectedGender(event.target.value);
	}

	useEffect(loadData, []);

	const { username, mail, birthdate, description, thumbnail } = user;

	return (
		<div className="container">
			<div className="row align-items-center justify-content-center">
				<div className="col-sm-8">
					<h5>Modification du compte</h5>
					<form className="updateForm" onSubmit={event => handleSubmit(event)}>
						<label htmlFor="sexe">Sexe</label>
						<div className="form-group">
							<div className="form-check form-check-inline">
								<input
									className="form-check-input"
									type="radio"
									name="inlineRadioOptions"
									id="sexe1"
									value="0"
									checked={selectedGender === '0'}
									onChange={handleGenderChange}
								/>
								<label className="form-check-label" htmlFor="inlineRadio1">
									Homme
								</label>
							</div>
							<div className="form-check form-check-inline">
								<input
									className="form-check-input"
									type="radio"
									name="inlineRadioOptions"
									id="sexe2"
									value="1"
									checked={selectedGender === '1'}
									onChange={handleGenderChange}
								/>
								<label className="form-check-label" htmlFor="inlineRadio2">
									Femme
								</label>
							</div>
						</div>
						<div className="form-group">
							<label htmlFor="inputMail">Adresse mail</label>
							<input
								type="text"
								className="form-control"
								id="inputMail"
								placeholder={mail}
								ref={emailInput}
							/>
						</div>
						<div className="form-group">
							<label htmlFor="inputName">Chooser Name</label>
							<input
								type="text"
								className="form-control"
								id="inputName"
								placeholder={username}
								ref={chooserNameInput}
							/>
						</div>
						<div className="form-group">
							<label htmlFor="date">Date de naissance</label>
							<input
								min="1970-01-01"
								max="2020-01-01"
								type="date"
								id="date"
								className="form-control"
								ref={birthdateInput}
								placeholder={birthdate}
							/>
						</div>
						<div className="form-group">
							<label htmlFor="inputPassword">Mot de passe</label>
							<input
								type="password"
								className="form-control"
								id="inputPassword"
								placeholder="***"
								ref={passwordInput}
							/>
						</div>
						<div className="form-group">
							<label className="custom-file-label" htmlFor="file">
								{isSelected ? <p>{selectedFile.name}</p> : <p> {thumbnail} </p>}
							</label>
							<input
								type="file"
								className="custom-file-input"
								id="file"
								accept=".png, .svg, .jpeg, .jpg"
								onChange={handleFileChange}
							/>
						</div>
						<div className="form-group">
							<label htmlFor="description" id="insertion">
								Description du profil
							</label>
							<textarea
								id="description"
								cols="30"
								rows="5"
								className="form-control"
								ref={profilDescriptionInput}
								placeholder={description}
							></textarea>
						</div>
						<button type="submit" className="btn btn-primary">
							Enregistrer
						</button>
					</form>
				</div>
			</div>
		</div>
	);
}
