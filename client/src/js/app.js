import { render } from 'react-dom';
import Menu from './Menu';
import Navigator from './Navigator';
import { BrowserRouter } from 'react-router-dom';
import AddButton from './AddButton';
render(
	<BrowserRouter>
		<Menu />
		<div id="space">

		</div>
		<AddButton />
		<Navigator />
	</BrowserRouter>,
	document.querySelector('.appContainer')
);
