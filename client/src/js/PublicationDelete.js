import { useHistory } from 'react-router';
import isLoggedIn from './is_logged_in';
import store from 'store';

export default function PublicationDelete(props) {
	if (!isLoggedIn()) {
		return <Redirect to="/login" />;
	}
	const { id_user, id_publication, filename } = props;
	const history = useHistory();

	function hanleClick() {
		fetch('http://localhost:8080/publications', {
			credentials: 'include',
			headers: {
				Accept: 'application/json',
				'Content-Type': 'application/json',
			},
			method: 'DELETE',
			body: JSON.stringify({
				id_publication: id_publication,
				filename: filename,
			}),
		})
			.then(result => result.json())
			.then(data => {
				if (data.message == 'success') {
					history.push('/choozer/' + id_user);
				} else {
					alert(data.message);
				}
			});
	}

	let isLoading = 'is-loading';
	if (store.get('id') == id_user) {
		isLoading = '';
	}
	return (
		<div className={isLoading}>
			<button
				type="button"
				className="btn btn-primary delete_publication"
				data-dismiss="modal"
				aria-label="Close"
				onClick={() => hanleClick()}
			>
				Supprimer
			</button>
		</div>
	);
}
