import { Component, useEffect, useState } from 'react';
import PublicationThumbail from './PublicationThumbnail';
import isLoggedIn from './is_logged_in';

/**
 * fonction permet de generer plusieurs publicaitonList
 * @param {object} props
 * passage en parametre d'un props comportant toutes les publications a afficher
 * @returns
 */
export default function PublicationsList(props) {
	if (!isLoggedIn()) {
		return <Redirect to="/login" />;
	}
	
	return (
		<div>
			{props.publications.map(publication => (
				<PublicationThumbail key={publication.id} publication={publication} />
			))}
		</div>
	);
    
}
