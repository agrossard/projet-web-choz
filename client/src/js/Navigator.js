import { Switch, Route } from 'react-router-dom';
import JeuChooz from './JeuChooz';
import PostForm from './PostForm';
import LoginForm from './LoginForm';
import RegisterForm from './RegisterForm';
import Feeds from './Feeds';
import CategorieList from './CategorieList';
import Explorer from './Explorer';
import CategorieDetails from './CategorieDetails';
import ProfileUser from './ProfileUser';
import EditUser from './EditUser';

export default function Navigator() {
	return (
		<Switch>
			<Route exact path="/login">
				<LoginForm />
			</Route>
			<Route exact path="/">
				<JeuChooz />
			</Route>
			<Route exact path="/publications/add">
				<PostForm />
			</Route>
			<Route exact path="/register">
				<RegisterForm />
			</Route>
			<Route exact path="/feeds">
				<Feeds />
			</Route>
			<Route exact path="/explorer">
				<Explorer />
			</Route>
			<Route exact path="/categories/:id">
				<CategorieDetails />
			</Route>
			<Route exact path="/choozer/edit">
				<EditUser />
			</Route>
			<Route exact path="/choozer/:id">
				<ProfileUser />
			</Route>
		</Switch>
	);
}
