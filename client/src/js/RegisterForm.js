import { useRef, useState } from 'react';
import { useHistory } from 'react-router-dom';
import store from 'store';

/**
 * Formulaire d'inscription pour un nouveau compte
 * @returns redirection et message de confirmationx
 */
export default function RegisterForm() {
	// chargement du formulaire
	const [selectedGender, setSelectedGender] = useState('');
	const [selectedFile, setSelectedFile] = useState();
	const [isSelected, setIsSelected] = useState(false);

	// références aux informations de l'utilisateur
	const emailInput = useRef();
	const chooserNameInput = useRef();
	const birthdateInput = useRef();
	const passwordInput = useRef();
	const profilDescriptionInput = useRef();

	const history = useHistory();

	/**
	 * A la fin du formulaire, on envoi les informations à l'API
	 * @param {Evenement lors du clique} event
	 */
	function handleSubmit(event) {
		event.preventDefault();

		// split la date pour recuperer un timestamp
		const date = birthdateInput.current.value;
		const [year, month, day] = date.split('-');

		// genere le formData que l'on envoi avec la methode post
		const formData = new FormData();
		formData.append('username', chooserNameInput.current.value);
		formData.append('mail', emailInput.current.value);
		formData.append('password', passwordInput.current.value);
		formData.append(
			'birthdate',
			parseInt(new Date(year, month - 1, day, 22, 0, 0).getTime() / 1000)
		);
		formData.append('sexe', selectedGender);
		formData.append('description', profilDescriptionInput.current.value);
		formData.append('image', selectedFile);

		// envoi du formulaire d'inscription de l'utilisateur
		fetch('http://localhost:8080/users/register', {
			method: 'POST',
			body: formData,
			credentials: 'include',
		})
			.then(response => response.json())
			.then(data => {
				if (data.user != null) {
					store.set('loggedIn', true);
					store.set('id', data.user);
					return <Redirect to="/login" />;
				} else {
					alert('Erreur de mail ou de mot de passe');
				}
			})
			.catch(err => {
				alert(err);
			});
	}

	/**
	 *
	 * @param {Changemement d'etat du fichier} changeEvent
	 */
	function handleFileChange(event) {
		setSelectedFile(event.target.files[0]);
		setIsSelected(true);
	}

	/**
	 *
	 * @param {Changement de radio boutton selectionné} changeEvent
	 */
	function handleGenderChange(event) {
		setSelectedGender(event.target.value);
	}

	return (
		<div className="container">
			<div className="row align-items-center justify-content-center">
				<div className="col-sm-8">
					<h5>Création du compte</h5>
					<form
						className="registerForm"
						onSubmit={event => handleSubmit(event)}
					>
						<label htmlFor="sexe">Sexe</label>
						<div className="form-group">
							<div className="form-check form-check-inline">
								<input
									className="form-check-input"
									type="radio"
									name="inlineRadioOptions"
									id="sexe1"
									value="0"
									required
									checked={selectedGender === '0'}
									onChange={handleGenderChange}
								/>
								<label className="form-check-label" htmlFor="inlineRadio1">
									Homme
								</label>
							</div>
							<div className="form-check form-check-inline">
								<input
									className="form-check-input"
									type="radio"
									name="inlineRadioOptions"
									id="sexe2"
									value="1"
									checked={selectedGender === '1'}
									onChange={handleGenderChange}
								/>
								<label className="form-check-label" htmlFor="inlineRadio2">
									Femme
								</label>
							</div>
						</div>
						<div className="form-group">
							<label htmlFor="inputMail">Adresse mail</label>
							<input
								type="email"
								className="form-control"
								id="inputMail"
								placeholder="exemple@mail"
								ref={emailInput}
								required
							/>
						</div>
						<div className="form-group">
							<label htmlFor="inputName">Chooser Name</label>
							<input
								type="text"
								className="form-control"
								id="inputName"
								placeholder="Dembo"
								ref={chooserNameInput}
								required
							/>
						</div>
						<div className="form-group">
							<label htmlFor="date">Date de naissance</label>
							<input
								required
								min="1970-01-01"
								max="2020-01-01"
								type="date"
								id="date"
								className="form-control"
								ref={birthdateInput}
							/>
						</div>
						<div className="form-group">
							<label htmlFor="inputPassword">Mot de passe</label>
							<input
								required
								type="password"
								className="form-control"
								id="inputPassword"
								placeholder="1234"
								ref={passwordInput}
							/>
						</div>
						<div className="form-group">
							<label className="custom-file-label" htmlFor="file">
								{isSelected ? (
									<p>{selectedFile.name}</p>
								) : (
									<p> Photo de profil </p>
								)}
							</label>
							<input
								required
								type="file"
								className="custom-file-input"
								id="file"
								accept=".png, .svg, .jpeg, .jpg"
								onChange={handleFileChange}
							/>
						</div>
						<div className="form-group">
							<label htmlFor="description" id="insertion">
								Description du profil
							</label>
							<textarea
								required
								id="description"
								cols="30"
								rows="5"
								className="form-control"
								ref={profilDescriptionInput}
							></textarea>
						</div>
						<button type="submit" className="btn btn-primary">
							S'enregistrer
						</button>
					</form>
				</div>
			</div>
		</div>
	);
}
