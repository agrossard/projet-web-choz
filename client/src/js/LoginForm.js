import { useRef } from 'react';
import { Link, useHistory, Redirect } from 'react-router-dom';
import store from 'store';
import isLoggedIn from './is_logged_in';

/**
 * Formulaire de connexion
 * @returns message de validation et redirection si validé
 */
export default function LoginForm() {
	// référence au login et mot de passe de l'user
	const loginInput = useRef();
	const passwordInput = useRef();

	const history = useHistory();

	if (isLoggedIn()) {
		return <Redirect to="/" />;
	}

	/**
	 * A la fin du formulaire, on envoi les informations à l'API
	 * @param {Evenement lors du clique} event
	 */
	function handleSubmit(event) {
		event.preventDefault();

		// demande de connexion de l'utilisateur
		fetch('http://localhost:8080/users/login', {
			method: 'POST',
			credentials: 'include',
			headers: {
				Accept: 'application/json',
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({
				login: loginInput.current.value,
				password: passwordInput.current.value,
			}),
		})
			.then(response => response.json())
			.then(response => {
				console.log(response);
				if (response.accessToken) {
					fetch('http://localhost:8080/jwtid', {
						method: 'GET',
						credentials: 'include',
					})
						.then(response => response.json())
						.then(response => {
							store.set('loggedIn', true);
							store.set('id', response);
							history.push('/');
						});
				} else {
					alert(response.message);
				}
			})
			.catch(err => {
				alert(err);
			});
	}

	return (
		<div className="container">
			<div className="row align-items-center justify-content-center">
				<div className="col-sm-8">
					<h5>Connexion</h5>
					<form className="loginForm" onSubmit={event => handleSubmit(event)}>
						<div className="form-group">
							<label htmlFor="inputMail">Adresse mail</label>
							<input
								type="text"
								className="form-control"
								id="inputMail"
								placeholder="exemple@mail"
								ref={loginInput}
							/>
						</div>
						<div className="form-group">
							<label htmlFor="inputPassword">Mot de passe</label>
							<input
								type="password"
								className="form-control"
								id="inputPassword"
								placeholder="1234"
								ref={passwordInput}
							/>
						</div>
						<button type="submit" className="btn btn-primary">
							Se connecter
						</button>
						<Link to="/register">
							<small id="register" className="form-text text-muted text-center">
								Besoin d'un compte ? S'inscrire sur Chooz
							</small>
						</Link>
					</form>
				</div>
			</div>
		</div>
	);
}
