const express = require('express');
const cookieParser = require('cookie-parser');
const cors = require('cors');
const fileUpload = require('express-fileupload');
const db = require('./models');

const router = express.Router();
const usersRoutes = require('./routes/users.routes');
const choozRoutes = require('./routes/chooz.routes');
const gameRoutes = require('./routes/game.routes');
const categoriesRoutes = require('./routes/categories.routes');
const publicationsRoutes = require('./routes/publications.routes');
const followerRoutes = require('./routes/followers.routes');
const { checkUser, requireAuth } = require('./authentication/authenticator');

const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(checkUser);
app.use(fileUpload({ createParentPath: true }));

var corsOptions = {
	origin: 'http://localhost:8000',
	credentials: true,
	exposedHeaders: ['set-cookie'],
};
app.use(cors(corsOptions));

app.get('/jwtid', requireAuth, (req, res) => {
	res.status(200).send(res.locals.user.id.toString());
});

app.get('/ping', function (req, res) {
	return res.status(200).send('pong');
});

app.use('/game', gameRoutes);
app.use('/chooz', choozRoutes);
app.use('/users', usersRoutes);
app.use('/categories', categoriesRoutes);
app.use('/publications', publicationsRoutes);
app.use('/follow', followerRoutes);

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
app.use('/api/v1', router);

db.config().then(() => app.listen(process.env.PORT || 8080));
