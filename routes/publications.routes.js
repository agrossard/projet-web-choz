const publications = require('../controllers/publications.controller.js');
const { requireAuth } = require('../authentication/authenticator');

var router = require('express').Router();

// Retrieve all publication
//router.get('/', publications.findAll);
//router.get('/categories', publications.findAllWithCategories);
router.post('/users', requireAuth, publications.findAllWithUsers);
router.post(
	'/users/categories',
	requireAuth,
	publications.findAllWithUsersAndCategories
);
router.get('/random', requireAuth, publications.findRandom);

router.post('/', requireAuth, publications.create);
router.post('/search', requireAuth, publications.search);
router.delete('/', requireAuth, publications.remove);

module.exports = router;
