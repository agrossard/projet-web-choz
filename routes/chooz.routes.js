const { requireAuth } = require('../authentication/authenticator');
const chooz = require('../controllers/chooz.controller.js');

var router = require('express').Router();

// Retrieve all Tutorials
router.get('/', requireAuth, chooz.findAll);
router.post('/', requireAuth, chooz.create);
router.post('/categories', requireAuth, chooz.findTopByCategorie);
router.post('/publication', requireAuth, chooz.counterPublication);
router.post('/users', requireAuth, chooz.counterUser);

module.exports = router;
