const router = require('express').Router();
const { requireAuth } = require('../authentication/authenticator');
const followersController = require('../controllers/followers.controller');

router.post('/getFollowers', followersController.getFollowers);
router.post('/getFollowing', followersController.getFollowing);
router.post('/isFollowers', followersController.isFollowers);
router.post('/follow', followersController.follow);
router.post('/unfollow', followersController.unfollow);

module.exports = router;
