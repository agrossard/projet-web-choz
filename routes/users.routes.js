const router = require('express').Router();
const { requireAuth } = require('../authentication/authenticator');
const authController = require('../controllers/auth.controller');
const userController = require('../controllers/users.controller.js');

router.get('/', requireAuth, userController.getAll);
router.get('/logout', authController.logout);
router.get('/:id', requireAuth, userController.findOne);

router.post('/register', authController.signUp);
router.post('/login', authController.signIn);
router.post('/', requireAuth, userController.update);
router.post('/update', requireAuth, userController.update);

module.exports = router;
