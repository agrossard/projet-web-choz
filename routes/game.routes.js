const game = require('../controllers/game.controller.js');
const { requireAuth } = require('../authentication/authenticator');

var router = require('express').Router();

// Retrieve all Tutorials
router.get('/', requireAuth, game.startGame);

module.exports = router;
