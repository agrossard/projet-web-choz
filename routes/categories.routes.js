const { requireAuth } = require('../authentication/authenticator');
const categories = require('../controllers/categories.controller.js');

var router = require('express').Router();

// Retrieve all Tutorials
router.get('/', requireAuth, categories.findAll);
router.post('/users', requireAuth, categories.findCategoriesByUser);

module.exports = router;
