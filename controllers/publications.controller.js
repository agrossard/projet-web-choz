const { getTimeInTimestamp } = require('../utils/time.utils');
const { uploadImage, deleteImage } = require('../utils/image.utils');
const model = require('../models/index');
const { Op } = require('sequelize');
const Sequelize = require('sequelize');

/**
 * Retrieve all publications with associations from the database.
 * @param {*} req
 * @param {*} res
 */
/*
exports.findAll = (req, res) => {
	model.Publications.findAll({
		include: [
			{
				model: model.Categories,
			},
			{ model: model.Users },
		],
	})
		.then(data => {
			return res.send(data);
		})
		.catch(err => {
			return res.status(500).send({
				message:
					err.message || 'Some error occurred while retrieving tutorials.',
			});
		});
};
*/
/**
 * Retrieve all publications with users associations from the database
 * @param {*} req
 * @param {*} res
 */
exports.findAllWithUsers = (req, res) => {
	if (!req.body.id_user) {
		return res.status(500).send({
			message: 'Content can not be empty!',
		});
	}
	model.Publications.findAll({
		include: [
			{
				model: model.Categories,
			},
			{
				model: model.Users,
				where: { id: req.body.id_user },
			},
		],

		order: [['date', 'ASC']],
	})
		.then(data => {
			res.send(data);
		})
		.catch(err => {
			return res.status(500).send({
				message:
					err.message || 'Some error occurred while retrieving publications.',
			});
		});
};

/**
 * Retrieve all publications with users associations from the database
 * @param {*} req
 * @param {*} res
 */
exports.findAllWithUsersAndCategories = (req, res) => {
	if (!req.body.id_user || !req.body.id_categorie) {
		return res.status(500).send({
			message: 'Content can not be empty!',
		});
	}
	model.Publications.findAll({
		include: [
			{
				model: model.Categories,
				where: { id: req.body.id_categorie },
			},
			{
				model: model.Users,
				where: { id: req.body.id_user },
			},
		],
		order: [['date', 'ASC']],
	})
		.then(data => {
			res.send(data);
		})
		.catch(err => {
			return res.status(500).send({
				message:
					err.message || 'Some error occurred while retrieving publications.',
			});
		});
};

/**
 * Retrieve all publications with categories associations where these publications aren't chooz
 * @param {*} req
 * @param {*} res
 * @returns
 */
exports.findForGame = async (req, res) => {
	const choozed_publications = JSON.parse(
		JSON.stringify(req.body.choozed_publications)
	);
	return await model.Publications.findAll({
		attributes: ['id'],
		where: {
			[Op.not]: { [Op.or]: choozed_publications },
		},

		include: {
			model: model.Categories,
			where: {
				id: req.body.id_categorie,
			},
		},
		order: [Sequelize.fn('RAND')],
		limit: 2,
	});
};

exports.counter = async (req, res) => {
	return await model.Publications.findAll({
		attributes: [[Sequelize.fn('COUNT', Sequelize.col('id')), 'nb_pub']],
	});
};
/**
 * fonction qui permet de renvoyer la liste des publications de maniere aleatoire
 * @param {*} req
 * @param {*} res
 */
exports.findRandom = async (req, res) => {
	let limit = 0;
	try {
		limit = JSON.parse(JSON.stringify(await this.counter(req, res)))[0].nb_pub;
	} catch (err) {
		return res.status(500).send({
			message: 'Some error occurred while retrieving number of publications.',
		});
	}

	model.Publications.findAll({
		include: [
			{
				model: model.Categories,
				attributes: ['id', 'name'],
			},
			{
				model: model.Users,
				attributes: ['id', 'username', 'thumbnail'],
			},
		],
		order: [Sequelize.fn('RAND')],
		litmit: limit,
	})
		.then(data => {
			res.status(200).send(data);
		})
		.catch(err => {
			res.status(500).send({
				message: 'Some error occurred while retrieving random publications.',
			});
		});
};

/**
 * fonction qui permet de rechercher une publication en fonction d'une chaine de caractère
 * @param {*} req
 * @param {*} res
 * @returns
 */
exports.search = (req, res) => {
	if (!req.body.words) {
		return res.status(500).send({
			message: 'Content can not be empty!',
		});
	}
	const array_words = req.body.words.split(' ');

	model.Publications.findAll({
		where: {
			description: {
				[Op.regexp]: array_words.join('|'),
			},
		},
		include: [
			{
				model: model.Users,
			},
			{
				model: model.Categories,
			},
		],
	})
		.then(data => {
			return res.status(200).send(data);
		})
		.catch(err => {
			return res.status(500).send({
				message: 'Some error occurred while retrieving publications.',
			});
		});
};
/**
 * Create a publication with users and categories associations which already created
 * @param {*} req
 * @param {*} res
 * @returns
 */
exports.create = async (req, res) => {
	if (
		!req.body.description ||
		!req.files ||
		!req.body.categories ||
		!req.body.exifs
	) {
		return res.status(500).send({
			message: 'Content can not be empty!',
		});
	}

	const image = req.files.image;

	const uploadPath =
		__dirname + '/../client/src/assets/images/publications/' + image.name;

	const date = getTimeInTimestamp();

	const id_categories = JSON.parse(req.body.categories);

	const publication = {
		pathfile: image.name,
		description: req.body.description,
		meta_data: req.body.exifs,
		date: date,
		id_user: res.locals.user.id,
	};

	const res_uploadImage = await uploadImage(uploadPath, image);
	if (res_uploadImage != '200') {
		return res.status(500).send(res_uploadImage);
	}

	model.Publications.create(publication)
		.then(data => {
			const id_publication = data.toJSON().id;

			id_categories.forEach(id_categorie => {
				const publication_categorie = {
					id_publication: id_publication,
					id_categorie: id_categorie,
				};

				model.Publications_Categories.create(publication_categorie).catch(
					err => {
						return res.status(500).send({
							message: err.message || 'Impossible to link with categories',
						});
					}
				);
			});
			return res.status(200).send({ message: 'success' });
		})
		.catch(err => {
			return res.status(500).send({
				message: err.message || 'Impossible to create the publication',
			});
		});
};

/**
 * fonction permettant de supprimer une publication
 * @param {*} req
 * @param {*} res
 * @returns
 */
exports.remove = async (req, res) => {
	if (!req.body.id_publication || !req.body.filename) {
		return res.status(500).send({
			message: 'Content can not be empty!',
		});
	}

	const deletePath =
		__dirname +
		'/../client/src/assets/images/publications/' +
		req.body.filename;
	const res_deleteImage = await deleteImage(deletePath);

	if (res_deleteImage != '200') {
		return res.status(500).send(res_deleteImage);
	}

	model.Publications.destroy({
		where: { id: req.body.id_publication },
		truncate: false,
		include: {
			model: model.Categories,
		},
	}).then(data => {
		return res.status(200).send({ message: 'success' });
	});
};
