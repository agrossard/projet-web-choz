const model = require('../models/index');
const { Op } = require('sequelize');

exports.follow = (req, res) => {
	try {
		model.Followers.create({
			id_user: req.body.id_user,
			following: req.body.following,
		}).then(result => {
			return res.sendStatus(200);
		});
	} catch (err) {
		return res.status(404).send(err);
	}
};

exports.getFollowers = (req, res) => {
	model.Followers.findAll({ where: { id_user: req.body.id_user } })
		.then(followers => {
			res.status(200).send(followers);
		})
		.catch(err => {
			res.send(err);
		});
};

exports.isFollowers = (req, res) => {
	if (!req.body.id_user || !req.body.followers) {
		return res.status(500).send({
			message: 'Content can not be empty!',
		});
	}

	model.Followers.findAll({
		where: {
			[Op.and]: [
				{
					id_user: req.body.id_user,
				},
				{
					following: req.body.followers,
				},
			],
		},
	})
		.then(data => {
			return res
				.status(200)
				.send(data.length == 0 ? { message: false } : { message: true });
		})
		.catch(err => {
			res.send(err);
		});
};

exports.unfollow = (req, res) => {
	model.Followers.destroy({
		where: {
			id_user: req.body.id_user,
			following: req.body.following,
		},
	})
		.then(data => {
			return res.sendStatus(200)
		})
		.catch(err => {
			return res.send(err);
		});
};

exports.getFollowing = (req, res) => {
	model.Followers.findAll({ where: { following: req.body.id_user } })
		.then(followers => {
			res.status(200).send(followers);
		})
		.catch(err => {
			res.send(err);
		});
};
