const model = require('../models/index');
const jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
const config = require('../config/auth.config.js');
const { signUpErrors, signInErrors } = require('../utils/errors.utils');
const { Op } = require('sequelize');
const { uploadImage } = require('../utils/image.utils');

const maxAge = 3 * 24 * 60 * 60 * 1000;

module.exports.signUp = async (req, res) => {
	const { username, mail, password, birthdate, sexe, description } = req.body;

	const image = req.files.image;
	const uploadPath = __dirname + '/../client/src/assets/images/users/' + image.name;

	const res_uploadImage = await uploadImage(uploadPath, image);
	if (res_uploadImage != '200') {
		return res.status(500).send(res_uploadImage);
	}

	try {
		const user = await model.Users.create({
			username,
			mail,
			password: bcrypt.hashSync(password, 8),
			birthdate,
			sexe,
			description,
			thumbnail: image.name,
		});
		
		var token = jwt.sign({ id: user.null }, config.secret, {
			expiresIn: maxAge,
		});

		res.cookie('jwt', token, { httpOnly: true, maxAge });
		res.status(201).json({ user: user.null, accessToken: token });
	} catch (err) {
		const errors = signUpErrors(err);
		res.status(200).send({ errors });
	}
};

module.exports.signIn = async (req, res) => {
	const { login } = req.body;

	model.Users.findOne({
		where: {
			[Op.or]: [{ mail: login }, { username: login }],
		},
	})
		.then(user => {
			if (!user) {
				res.status(404).send({ message: 'User Not found.' });
			}

			var passwordIsValid = bcrypt.compareSync(
				req.body.password,
				user.password
			);

			if (!passwordIsValid) {
				return res.status(401).send({
					accessToken: null,
					message: 'Invalid Password!',
				});
			}
			var token = jwt.sign({ id: user.id }, config.secret, {
				expiresIn: maxAge,
			});

			res.cookie('jwt', token, { httpOnly: true, maxAge });

			res.status(200).send({
				id: user.id,
				username: user.username,
				mail: user.mail,
				accessToken: token,
			});
		})
		.catch(err => {
			const errors = signInErrors(err);
			res.status(500).send(errors);
		});
};

module.exports.logout = (req, res) => {
	console.log('logout');
	res.cookie('jwt', '', { maxAge: 1 });
	res.redirect('/');
};
