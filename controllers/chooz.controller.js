const model = require('../models/index');
const { getTimeInTimestamp } = require('../utils/time.utils');
const Sequelize = require('sequelize');
const { Op } = require('sequelize');

// Retrieve all publications with associations from the database.
exports.findAll = (req, res) => {
	model.Chooz.findAll({
		include: {
			model: model.Publications,
		},
	})
		.then(data => {
			return res.send(data);
		})
		.catch(err => {
			return res.status(500).send({
				message: err.message || 'Some error occurred while retrieving chooz.',
			});
		});
};

/**
 * fonction permettant d'afficher les publications par ordre de la plus choozé et en fonction d'une categorie selectionne
 * @param {*} req
 * @param {*} res
 * @returns
 */
exports.findTopByCategorie = (req, res) => {
	if (!req.body.id_categorie) {
		return res.status(500).send({
			message: 'Content can not be empty',
		});
	}

	model.Chooz.findAll({
		attributes: [[Sequelize.fn('COUNT', Sequelize.col('chooz.id')), 'chooz']],
		include: {
			model: model.Publications,
			require: true,
			include: [
				{
					require: true,
					model: model.Categories,
					where: { id: req.body.id_categorie },
				},
				{
					require: true,
					model: model.Users,
				},
			],
		},
		group: ['chooz.id_publication'],
		//where: Sequelize.where(Sequelize.col(`publication`), Op.ne, null)
		order: [[Sequelize.literal('chooz'), 'DESC']],
	}).then(data => {
		const publications = [];	
		data.forEach(publication => {
			if(publication.publication != null){
				publications.push(publication.publication)
			}
		});
		res.status(200).send(publications);
	}).catch(err => {
		return res.status(500).send({
			message: 'Some error occurred while retieving count chooz for users',
		});
	})
};
/**
 * fonction permettant de recuperer tous les chooz fait d'un utilisateur
 * @param {*} req
 * @param {*} res
 * @returns
 */
exports.findAllByUsers = async (req, res) => {
	return await model.Chooz.findAll({
		attributes: [['id_publication', 'id']],
		where: {
			id_user: req.body.id_user,
		},
	});
};

/**
 * fonction permettant de counter le nombre de chooz qu'a fait un utilisateur
 * @param {*} req
 * @param {*} res
 * @returns
 */
exports.counterUser = async (req, res) => {
	req.body.id_user = req.body.id_user
	try {
		let counterUser = null;
		counterUser = JSON.parse(
			JSON.stringify(await this.findAllByUsers(req, res))
		).length;
		return res.status(200).send({ chooz: counterUser });
	} catch (err) {
		return res.status(500).send({
			message: 'Some error occurred while retieving count chooz for users',
		});
	}
};

/**this
 * fonction permettant de compter le nombre de chooz pour une publication donné
 * @param {*} req
 * @param {*} res
 * @returns
 */
exports.counterPublication = (req, res) => {
	if (!req.body.id_publication) {
		return res.status(500).send({
			message: 'Content can not be empty',
		});
	}

	model.Chooz.findAll({
		attributes: [
			[Sequelize.fn('COUNT', Sequelize.col('id_publication')), 'chooz'],
		],
		where: { id_publication: req.body.id_publication },
	})
		.then(data => {
			return res.status(200).send(data);
		})
		.catch(err => {
			return res.status(500).send({
				message:
					'Some error occurred while retieving count chooz for publication',
			});
		});
};

/**
 * fonction permettant d'enregistrer le choix d'une personne entre 2 publications (chooz)
 * @param {*} req
 * @param {*} res
 * @returns
 */
exports.create = (req, res) => {
	if (!req.body.id_publication || !req.body.id_game) {
		return res.status(500).send({
			message: 'Content can not be empty',
		});
	}
	const date = getTimeInTimestamp();

	const chooz = {
		id_user: res.locals.user.id,
		id_publication: req.body.id_publication,
		id_game: req.body.id_game,
		date: date,
	};

	model.Chooz.create(chooz)
		.then(data => {
			res.status(200).send({ message: 'success' });
		})
		.catch(err => {
			return res.status(500).send({
				message: err.message || 'Some error occurred while creating chooz',
			});
		});
};
