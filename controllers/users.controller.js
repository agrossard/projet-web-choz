const model = require('../models/index');

// Retrieve all Tutorials from the database.
exports.getAll = (req, res) => {
	model.Users.findAll()
		.then(users => {
			res.status(200).json(users);
		})
		.catch(error => {
			res.status(400).send(error);
		});
};

// Find a single Tutorial with an id
exports.findOne = (req, res) => {
	const id = req.params.id;

	model.Users.findByPk(id)
		.then(data => {
			res.status(200).send(data);
		})
		.catch(err => {
			res.status(500).send({
				message: 'Error retrieving Tutorial with id=' + id,
			});
		});
};

exports.update = (req, res) => {
	const id = res.locals.user.id;
	const { username, mail, password, birthdate, sexe, description, thumbnail } =
		req.body;
	console.log({
		username,
		mail,
		password,
		birthdate,
		sexe,
		description,
		thumbnail,
	});

	console.log(typeof username);

	model.Users.update(
		{
			username,
			mail,
			password,
			birthdate: birthdate === 'NaN' ? '' : birthdate,
			sexe,
			description,
			thumbnail,
		},
		{ where: { id: id } }
	)
		.then(response => res.sendStatus(200).send(response))
		.catch(err => console.log(err));
};
