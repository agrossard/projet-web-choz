const model = require('./../models/index');
const Sequelize = require('sequelize');

// Retrieve all users from the database.
exports.findAll = (req, res) => {
	model.Categories.findAll({
		order: [['name', 'asc']],
	})
		.then(data => {
			res.send(data);
		})
		.catch(err => {
			res.status(500).send({
				message:
					err.message || 'Some error occurred while retrieving categories.',
			});
		});
};

/**
 * permet de renvoyer une categorie aleatoire
 * @param {*} req
 * @param {*} res
 * @returns
 */

exports.findRandom = async (req, res) => {
	return await model.Categories.findOne({
		include: {
			model: model.Publications,
			required: true,
		},

		order: [Sequelize.fn('RAND')],
	});
};

/**
 * fonction permettant de renvoyer la liste des categories pour un utilisateur
 * @param {*} req
 * @param {*} res
 * @returns
 */
exports.findCategoriesByUser = (req, res) => {
	if (!req.body.id_user) {
		return res.status(500).send({
			message: 'Content can not be empty!',
		});
	}

	model.Categories.findAll({
		attributes: ['id','name'],
		include: {
			model: model.Publications,
			attributes: [],
			required: true,
			include: {
				model: model.Users,
				attributes: [],
				where: { id: req.body.id_user },
			},
		},
		order: [['name', 'ASC']],
		group: ['categories.id'],
	})
		.then(data => {
			return res.status(200).send(data);
		})
		.catch(err => {
			return res.status(500).send({
				message: 'Some error occurred while retrieving categories.',
			});
		});
};
