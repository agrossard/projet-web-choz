const { getTimeInTimestamp } = require('../utils/time.utils');
const model = require('./../models/index');
const Publications = require('../controllers/publications.controller.js');
const Categories = require('../controllers/categories.controller.js');
const Chooz = require('../controllers/chooz.controller.js');

// Send and create a game from the database.
exports.startGame = async (req, res) => {
	const categorie = await Categories.findRandom(req, res);
	req.body.id_categorie = categorie.id;

	req.body.id_user = res.locals.user.id;
	let choozed_publications = null;
	try {
		choozed_publications = await Chooz.findAllByUsers(req, res);
	} catch (err) {
		return res
			.status(500)
			.send('Some error occurred while retrieving choozed_publications');
	}
	req.body.choozed_publications = choozed_publications;

	let publications = null;
	try {
		publications = await Publications.findForGame(req, res);
	} catch (err) {
		return res
			.status(500)
			.send('Some error occurred while retrieving publications for game');
	}

	if (!publications[0] || !publications[1]) {
		return res.status(500).send({ message: 'No more game' });
	}

	const game = {
		id_pub_first: publications[0].id,
		id_pub_second: publications[1].id,
		date: getTimeInTimestamp(),
		status: 0,
	};

	model.Game.create(game)
		.then(data => {
			model.Game.findAll({
				attributes: ['id'],
				where: { id: data.id },
				include: [
					{ model: model.Publications, as: 'pub_first' },
					{ model: model.Publications, as: 'pub_second' },
				],
			})
				.then(result => {
					return res.status(200).send(result);
				})
				.catch(err => {
					return res
						.status(500)
						.send('Some error occurred while retrieving game');
				});
		})
		.catch(err => {
			return res.status(500).send('Some error occurred while creating game');
		});
};
