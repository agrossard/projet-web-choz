/**
 * function qui permet de déposer une image au chemin donnée
 * @param {string} uploadPath
 *  chemin où sera déposé le fichier composé de son nom avec
 * @param {file} image
 *  image/fichier qui va être déposé
 */
module.exports.uploadImage = async (uploadPath, image) => {
	const extension = image.name.split('.')[1];
	const array_extensions = ['png', 'jpeg', 'svg', 'jpg'];

	if (array_extensions.indexOf(extension) < 0) {
		return { message: 'Image extension is denied' };
	}
	if (image.size > 10000000) {
		return { message: 'Image is too big' };
	}

	const promisify = require('util').promisify;
	const uploadFile = promisify(image.mv);

	return uploadFile(uploadPath)
		.then(() => {
			return Promise.resolve('200');
		})
		.catch(() => {
			return Promise.resolve({
				message: 'Impossible to upload this image',
			});
		});
};
/**
 * function qui permet de suprrimer une image
 * @param {string} deletePath
 *  chemin du fichier à supprimer
 */
module.exports.deleteImage = deletePath => {
	const fs = require('fs');
	const promisify = require('util').promisify;
	const deleteFile = promisify(fs.unlink);

	return deleteFile(deletePath)
		.then(() => {
			return Promise.resolve('200');
		})
		.catch(() => {
			return Promise.resolve({
				message: 'Impossible to delete this image',
			});
		});
};
