const jwt = require('jsonwebtoken');
const config = require('../config/auth.config.js');
const model = require('../models/index');

module.exports.checkUser = (req, res, next) => {
	const token = req.cookies.jwt;
	if (token) {
		jwt.verify(token, config.secret, async (err, decodedToken) => {
			if (err) {
				res.locals.user = null;
				res.cookie('jwt', '', { maxAge: 1 });
				next();
			} else {
				let user = await model.Users.findOne({
					where: { id: decodedToken.id },
				});
				res.locals.user = user;
				next();
			}
		});
	} else {
		res.locals.user = null;
		next();
	}
};

module.exports.requireAuth = (req, res, next) => {
	const token = req.cookies.jwt;
	console.log(req.cookies);
	console.log({ token });
	if (token) {
		jwt.verify(token, config.secret, async (err, decodedToken) => {
			if (err) {
				res.send(200).json('no token');
			} else {
				next();
			}
		});
	} else {
		res.sendStatus(401);
	}
};
