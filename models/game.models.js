/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
	return sequelize.define(
		'game',
		{
			id: {
				type: DataTypes.INTEGER,
				allowNull: false,
				primaryKey: true,
				primaryKey: true,
				comment: 'null',
				autoIncrement: true,
			},
			id_pub_first: {
				type: DataTypes.INTEGER,
				allowNull: false,
				comment: 'null',
				references: {
					model: 'publications',
					key: 'id',
				},
			},
			id_pub_second: {
				type: DataTypes.INTEGER,
				allowNull: false,
				comment: 'null',
				references: {
					model: 'publications',
					key: 'id',
				},
			},
			date: {
				type: DataTypes.INTEGER,
				allowNull: false,
				comment: 'null',
			},
			status: {
				type: DataTypes.INTEGER,
				allowNull: false,
				comment: 'null',
			},
		},
		{
			tableName: 'game',
			timestamps: false,
		}
	);
};
