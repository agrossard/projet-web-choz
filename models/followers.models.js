/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
	return sequelize.define(
		'followers',
		{
			id: {
				type: DataTypes.INTEGER,
				primaryKey: true,
			},
			id_user: {
				type: DataTypes.INTEGER,
				references: {
					model: 'users',
					key: 'id',
				},
			},
			following: {
				type: DataTypes.INTEGER,
				references: {
					model: 'users',
					key: 'id',
				},
			},
		},
		{
			tableName: 'followers',
			timestamps: false,
		}
	);
};
