/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('publications_categories', {
    'id': {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      primaryKey: true,
      comment: "null",
      autoIncrement: true
    },
    'id_publication': {
      type: DataTypes.INTEGER,
      allowNull: false,
      comment: "null",
      references: {
        model: 'publications',
        key: 'id'
      }
    },
    'id_categorie': {
      type: DataTypes.INTEGER,
      allowNull: false,
      comment: "null",
      references: {
        model: 'categories',
        key: 'id'
      }
    }
  }, {
    tableName: 'publications_categories',
    timestamps: false,

  });
};
