/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
	const publications = sequelize.define(
		'publications',
		{
			id: {
				type: DataTypes.INTEGER,
				allowNull: false,
				primaryKey: true,
				primaryKey: true,
				comment: 'null',
				autoIncrement: true,
			},
			id_user: {
				type: DataTypes.INTEGER,
				allowNull: false,
				comment: 'null',
				references: {
					model: 'users',
					key: 'id',
				},
			},
			date: {
				type: DataTypes.INTEGER,
				allowNull: false,
				comment: 'null',
			},
			description: {
				type: DataTypes.TEXT,
				allowNull: false,
				comment: 'null',
			},
			pathfile: {
				type: DataTypes.STRING(255),
				allowNull: false,
				comment: 'null',
			},
			meta_data: {
				type: DataTypes.TEXT,
				allowNull: false,
				comment: 'null',
			},
		},
		{
			tableName: 'publications',
			timestamps: false,
		}
	);



	return publications;
};
