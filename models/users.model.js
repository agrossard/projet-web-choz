module.exports = (sequelize, Sequelize) => {
	const Users = sequelize.define(
		'users',
		{
			id: {
				type: Sequelize.INTEGER,
				primaryKey: true,
			},
			username: {
				type: Sequelize.STRING,
			},
			mail: {
				type: Sequelize.STRING,
			},
			password: {
				type: Sequelize.STRING,
			},
			birthdate: {
				type: Sequelize.INTEGER,
			},
			sexe: {
				type: Sequelize.BOOLEAN,
			},
			description: {
				type: Sequelize.TEXT,
			},
			thumbnail: {
				type: Sequelize.STRING,
			},
			status: {
				type: Sequelize.STRING,
			},
			rank: {
				type: Sequelize.INTEGER,
			},
		},
		{
			timestamps: false,
		}
	);

	return Users;
};
