/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('chooz', {
    'id': {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      primaryKey: true,
      comment: "null",
      autoIncrement: true
    },
    'id_user': {
      type: DataTypes.INTEGER,
      allowNull: false,
      comment: "null",
      references: {
        model: 'users',
        key: 'id'
      }
    },
    'id_publication': {
      type: DataTypes.INTEGER,
      allowNull: false,
      comment: "null",
      references: {
        model: 'publications',
        key: 'id'
      }
    },
    'id_game': {
      type: DataTypes.INTEGER,
      allowNull: false,
      comment: "null",
      references: {
        model: 'game',
        key: 'id'
      }
    },
    'date': {
      type: DataTypes.INTEGER,
      allowNull: false,
      comment: "null"
    }
  }, {
    tableName: 'chooz',
    timestamps: false,
  });
};
