const config = require('../config/db.config.js');

const Sequelize = require('sequelize');
const sequelize = new Sequelize(config.DB, config.USER, config.PASSWORD, {
	host: config.HOST,
	port: config.PORT,
	dialect: config.dialect,

	pool: {
		max: config.pool.max,
		min: config.pool.min,
		acquire: config.pool.acquire,
		idle: config.pool.idle,
	},
});

exports.sequelize = sequelize;

exports.Users = require('./users.model.js')(sequelize, Sequelize);
exports.Game = require('./game.models.js')(sequelize, Sequelize);
exports.Chooz = require('./chooz.models.js')(sequelize, Sequelize);
exports.Followers = require('./followers.models.js')(sequelize, Sequelize);
exports.Categories = require('./categories.models.js')(sequelize, Sequelize);
exports.Publications = require('./publications.models.js')(
	sequelize,
	Sequelize
);
exports.Publications_Categories =
	require('./publications_categories.models.js')(sequelize, Sequelize);

exports.config = () =>
	sequelize
		.authenticate()
		.then(() => console.log('Connected to database!'))
		.catch(error => console.log('error', error));

// FOREIGN KEY AND ASSOCITION TABLE

///////////////
// Publications
///////////////
exports.Publications.belongsTo(exports.Users, {
	sourcekey: 'id',
	foreignKey: 'id_user',
});
///////
// GAME
///////
exports.Game.belongsTo(exports.Publications, {
	sourcekey: 'id',
	foreignKey: 'id_pub_first',
	as: 'pub_first',
});

exports.Game.belongsTo(exports.Publications, {
	sourcekey: 'id',
	foreignKey: 'id_pub_second',
	as: 'pub_second',
});
/////////
// CHOOZ
/////////
exports.Chooz.belongsTo(exports.Users,{
	sourcekey: 'id',
	foreignKey: 'id_user',
})

exports.Chooz.belongsTo(exports.Publications,{
	sourcekey: 'id',
	foreignKey: 'id_publication',
})

exports.Chooz.belongsTo(exports.Game,{
	sourcekey: 'id',
	foreignKey: 'id_game',
})
/////////////
// FOLLOWERS
/////////////
exports.Followers.belongsTo(exports.Users, {
	sourcekey: 'id',
	foreignKey: 'id_user',
});

exports.Followers.belongsTo(exports.Users, {
	sourcekey: 'id',
	foreignKey: 'following',
});
////////////////////////
// PUBLICATION_CATEGORIE
////////////////////////
exports.Publications.belongsToMany(exports.Categories, {
	through: exports.Publications_Categories,
	foreignKey: 'id_publication',
});

exports.Categories.belongsToMany(exports.Publications, {
	through: exports.Publications_Categories,
	foreignKey: 'id_categorie',
});
