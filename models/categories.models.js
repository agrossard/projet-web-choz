module.exports = function (sequelize, DataTypes) {
	return sequelize.define(
		'categories',
		{
			id: {
				type: DataTypes.INTEGER,
				allowNull: false,
				primaryKey: true,
				primaryKey: true,
				comment: 'null',
				autoIncrement: true,
			},
			name: {
				type: DataTypes.STRING(255),
				allowNull: false,
				comment: 'null',
			},
			thumbnail: {
				type: DataTypes.STRING(255),
				allowNull: false,
				comment: 'null',
			},
		},
		{
			tableName: 'categories',
			timestamps: false,
		}
	);
};
