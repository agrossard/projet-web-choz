{
  "swagger": "2.0",
  "info": {
    "description": "This is the swagger for Chooz project",
    "version": "1.0.0",
    "title": "Swagger Chooz",
    "contact": {
      "email": "chooz@agrossard.fr"
    }
  },
  "host": "localhost:8080",
  "tags": [
    {
      "name": "user",
      "description": "Operations about user"
    },
    {
      "name": "Followers",
      "description": "Operations to follow"
    }
  ],
  "paths": {
    "/users/register": {
      "post": {
        "tags": [
          "user"
        ],
        "summary": "Create user",
        "description": "This is used to create a user.",
        "operationId": "createUser",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "in": "body",
            "name": "body",
            "description": "Created user object",
            "required": true,
            "schema": {
              "$ref": "#/definitions/User"
            }
          }
        ],
        "responses": {
          "default": {
            "description": "successful operation"
          }
        }
      }
    },
    "/users/login": {
      "post": {
        "tags": [
          "user"
        ],
        "summary": "Logs user into the system",
        "description": "",
        "operationId": "loginUser",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "in": "body",
            "name": "body",
            "description": "Created user object",
            "required": true,
            "schema": {
              "type": "object",
              "required": [
                "login",
                "password"
              ],
              "properties": {
                "login": {
                  "type": "string",
                  "example": "rootroot"
                },
                "password": {
                  "type": "string",
                  "example": "123"
                }
              }
            }
          }
        ],
        "responses": {
          "200": {
            "description": "successful operation",
            "schema": {
              "type": "object",
              "properties": {
                "id": {
                  "type": "integer",
                  "example": 7
                },
                "username": {
                  "type": "string",
                  "example": "rootroot"
                },
                "mail": {
                  "type": "string",
                  "example": "rootroot"
                },
                "accessToken": {
                  "type": "string",
                  "example": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NywiaWF0IjoxNjIxMzUxMjcyLCJleHAiOjE4ODA1NTEyNzJ9.3msjlO-oVyRcT8S6A4vU1gKPL7YSbLh-5U-olqI5_bc"
                }
              }
            }
          },
          "400": {
            "description": "Invalid username/password supplied"
          }
        }
      }
    },
    "/user/logout": {
      "get": {
        "tags": [
          "user"
        ],
        "summary": "Logs out current logged in user session",
        "description": "",
        "operationId": "logoutUser",
        "produces": [
          "application/json"
        ],
        "parameters": [],
        "responses": {
          "default": {
            "description": "successful operation"
          }
        }
      }
    },
    "/users/{id}": {
      "get": {
        "tags": [
          "user"
        ],
        "summary": "Get user by id",
        "description": "",
        "operationId": "getUserByName",
        "produces": [
          "application/xml",
          "application/json"
        ],
        "parameters": [
          {
            "name": "id",
            "in": "path",
            "description": "The id that needs to be fetched. Use 1 for testing. ",
            "required": true,
            "type": "string"
          }
        ],
        "responses": {
          "200": {
            "description": "successful operation",
            "schema": {
              "$ref": "#/definitions/User"
            }
          },
          "400": {
            "description": "Invalid username supplied"
          },
          "404": {
            "description": "User not found"
          }
        }
      },
      "put": {
        "tags": [
          "user"
        ],
        "summary": "Updated user",
        "description": "This can only be done by the logged in user.",
        "operationId": "updateUser",
        "produces": [
          "application/xml",
          "application/json"
        ],
        "parameters": [
          {
            "name": "id",
            "in": "path",
            "description": "name that need to be updated",
            "required": true,
            "type": "string"
          },
          {
            "in": "body",
            "name": "body",
            "description": "Updated user object",
            "required": true,
            "schema": {
              "$ref": "#/definitions/User"
            }
          }
        ],
        "responses": {
          "400": {
            "description": "Invalid user supplied"
          },
          "404": {
            "description": "User not found"
          }
        }
      },
      "delete": {
        "tags": [
          "user"
        ],
        "summary": "Delete user",
        "description": "This can only be done by the logged in user.",
        "operationId": "deleteUser",
        "produces": [
          "application/xml",
          "application/json"
        ],
        "parameters": [
          {
            "name": "id",
            "in": "path",
            "description": "The name that needs to be deleted",
            "required": true,
            "type": "string"
          }
        ],
        "responses": {
          "400": {
            "description": "Invalid username supplied"
          },
          "404": {
            "description": "User not found"
          }
        }
      }
    },
    "/follow/getFollowers": {
      "get": {
        "tags": [
          "Followers"
        ],
        "summary": "Get all followers",
        "description": "Get all followers for a given user",
        "parameters": [
          {
            "in": "body",
            "name": "body",
            "description": "Get all followers",
            "required": true,
            "schema": {
              "type": "object",
              "required": [
                "id_user"
              ],
              "properties": {
                "id_user": {
                  "type": "string",
                  "example": 4
                }
              }
            }
          }
        ],
        "responses": {
          "200": {
            "description": "successful operation",
            "schema": {
              "type": "object",
              "properties": {
                "id": {
                  "type": "integer",
                  "example": 7
                },
                "id_user": {
                  "type": "string",
                  "example": 4
                },
                "following": {
                  "type": "string",
                  "example": 5
                }
              }
            }
          }
        }
      }
    },
    "/follow/getFollowing": {
      "get": {
        "tags": [
          "Followers"
        ],
        "summary": "Get all following",
        "description": "Get all following for a given user",
        "parameters": [
          {
            "in": "body",
            "name": "body",
            "description": "Get all following",
            "required": true,
            "schema": {
              "type": "object",
              "required": [
                "following"
              ],
              "properties": {
                "following": {
                  "type": "string",
                  "example": 4
                }
              }
            }
          }
        ],
        "responses": {
          "200": {
            "description": "successful operation",
            "schema": {
              "type": "object",
              "properties": {
                "id": {
                  "type": "integer",
                  "example": 7
                },
                "id_user": {
                  "type": "string",
                  "example": 4
                },
                "following": {
                  "type": "string",
                  "example": 5
                }
              }
            }
          }
        }
      }
    },
    "/follow/follow": {
      "post": {
        "tags": [
          "Followers"
        ],
        "summary": "Follow a user",
        "description": "Follow user",
        "parameters": [
          {
            "in": "body",
            "name": "body",
            "description": "Follow one user, the id of the user is in the cookies",
            "required": true,
            "schema": {
              "type": "object",
              "required": [
                "following"
              ],
              "properties": {
                "following": {
                  "type": "string",
                  "example": 4
                }
              }
            }
          }
        ],
        "responses": {
          "200": {
            "description": "successful operation",
            "schema": {
              "type": "object",
              "properties": {
                "id": {
                  "type": "integer",
                  "example": 7
                },
                "id_user": {
                  "type": "string",
                  "example": 4
                },
                "following": {
                  "type": "string",
                  "example": 5
                }
              }
            }
          }
        }
      }
    },
    "/follow/unfollow": {
      "post": {
        "tags": [
          "Followers"
        ],
        "summary": "Follow a user",
        "description": "Follow user",
        "parameters": [
          {
            "in": "body",
            "name": "body",
            "description": "Follow one user, the id of the user is in the cookies",
            "required": true,
            "schema": {
              "type": "object",
              "required": [
                "following"
              ],
              "properties": {
                "following": {
                  "type": "string",
                  "example": 4
                }
              }
            }
          }
        ],
        "responses": {
          "200": {
            "description": "successful operation",
            "schema": {
              "type": "object",
              "properties": {
                "id": {
                  "type": "integer",
                  "example": 7
                },
                "id_user": {
                  "type": "string",
                  "example": 4
                },
                "following": {
                  "type": "string",
                  "example": 5
                }
              }
            }
          }
        }
      }
    }
  },
  "definitions": {
    "Categories": {
      "type": "object",
      "properties": {
        "id": {
          "type": "integer",
          "format": "int64"
        },
        "name": {
          "type": "string"
        },
        "thumbnail": {
          "type": "string"
        }
      },
      "xml": {
        "name": "Categories"
      }
    },
    "Chooz": {
      "type": "object",
      "properties": {
        "id": {
          "type": "integer",
          "format": "int64"
        },
        "id_user": {
          "type": "integer"
        },
        "id_publication": {
          "type": "integer"
        },
        "id_game": {
          "type": "integer"
        },
        "date": {
          "type": "string"
        }
      },
      "xml": {
        "name": "Chooz"
      }
    },
    "Game": {
      "type": "object",
      "properties": {
        "id": {
          "type": "integer",
          "format": "int64"
        },
        "id_pub_first": {
          "type": "integer"
        },
        "id_pub_second": {
          "type": "integer"
        },
        "date": {
          "type": "string"
        },
        "status": {
          "type": "integer"
        }
      },
      "xml": {
        "name": "Game"
      }
    },
    "User": {
      "type": "object",
      "properties": {
        "id": {
          "type": "integer",
          "format": "int64"
        },
        "username": {
          "type": "string",
          "example": "test"
        },
        "mail": {
          "type": "string",
          "example": "test@me"
        },
        "password": {
          "type": "string",
          "example": "123"
        },
        "birthdate": {
          "type": "integer",
          "example": "1/1/1900"
        },
        "sexe": {
          "type": "boolean",
          "example": "1"
        },
        "description": {
          "type": "string",
          "example": "This is a user description"
        },
        "thumbnail": {
          "type": "string"
        },
        "status": {
          "type": "string"
        },
        "rank": {
          "type": "string"
        }
      },
      "xml": {
        "name": "Users"
      }
    },
    "Followers": {
      "type": "object",
      "properties": {
        "id": {
          "type": "integer"
        },
        "id_user": {
          "type": "integer"
        },
        "following": {
          "type": "integer"
        }
      },
      "xml": {
        "name": "Followers"
      }
    }
  },
  "externalDocs": {
    "description": "Find out more about Swagger",
    "url": "http://swagger.io"
  }
}